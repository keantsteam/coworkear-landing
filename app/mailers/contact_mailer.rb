class ContactMailer < ApplicationMailer

	default from: 'info@coworkear.com'
 
  def new_lead(lead)
    @lead = lead
    # mail(to: ['javier@coworkear.com'], subject: '[COWORKEAR] - New Lead Registrado')
    mail(to: ['javier@coworkear.com','alexis@coworkear.com','matias@coworkear.com'], subject: '[COWORKEAR] - New Lead Registrado')
  end

end
