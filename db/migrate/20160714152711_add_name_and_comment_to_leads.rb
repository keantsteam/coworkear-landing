class AddNameAndCommentToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :name, :string
    add_column :leads, :comment, :text
  end
end
